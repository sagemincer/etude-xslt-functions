from javax.xml.transform import TransformerFactory
from javax.xml.transform.stream import StreamSource, StreamResult
from java.io import File, StringWriter

stylesheet = StreamSource(File('with_xalan_java_func.xslt'))
factory = TransformerFactory.newInstance()
transformer = factory.newTransformer(stylesheet)
input_source = StreamSource(File('empty.xml'))
output_writer = StringWriter()
output_result = StreamResult(output_writer)
transformer.transform(input_source, output_result)
print(output_writer.toString())
