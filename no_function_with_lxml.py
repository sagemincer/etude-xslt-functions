from lxml.etree import parse as etparse
from lxml.etree import fromstring as etfromstring
from lxml.etree import XSLT, FunctionNamespace

stylesheet = XSLT(etparse(file('no_function_with_lxml.xslt')))
doc_root = etfromstring(file('empty.xml').read())
print(stylesheet(doc_root))
