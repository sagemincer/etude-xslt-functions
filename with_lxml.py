from lxml.etree import parse as etparse
from lxml.etree import fromstring as etfromstring
from lxml.etree import XSLT, FunctionNamespace

def five(context):
    return 5
functions = FunctionNamespace('http://example.com/ns/with_lxml')
functions['five'] = five
stylesheet = XSLT(etparse(file('with_lxml.xslt')))
doc_root = etfromstring(file('empty.xml').read())
print(stylesheet(doc_root))
