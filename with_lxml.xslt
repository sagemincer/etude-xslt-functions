<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:fivens="http://example.com/ns/with_lxml">
  <xsl:output method="xml" indent="yes" encoding="UTF-8" />

  <xsl:template match="/">
    <foo>
    here is the five:
    <xsl:value-of select="fivens:five()" />
    that was the five
    </foo>
  </xsl:template>
</xsl:stylesheet>
