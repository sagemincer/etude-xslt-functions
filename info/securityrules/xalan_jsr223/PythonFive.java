package info.securityrules.xalan_jsr223;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class PythonFive {
    public static String five() throws ScriptException {
	ScriptEngine engine = new ScriptEngineManager().getEngineByName("python");
	engine.eval("import xalan_jsr223_five_module");
	engine.eval("x = xalan_jsr223_five_module.five()");
	Object x = engine.get("x");
	return x.toString();
    }
}
