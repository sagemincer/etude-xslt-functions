XSLT functions
--------------

How do you make a custom function that can be called from an XSLT
stylesheet? This project is a study to find out. Pay attention not
only to its present form but also to its history.

The existing context is that sagemincer has a method of an object,
already written in Python, which is passed, bound, to lxml to use as a
custom XSLT function. This means the method gets to use self to alter
the state of the object, while having been called from the stylesheet.

The desired end state (which may or may not be possible) is to have
that same method of that same object be usable from within a
stylesheet being evaluated by Xalan, called from Jython.


Expected output
---------------

For all cases here, the expected output is an XML file that says
something like this (plus or minus whitespace)::

    <foo>
    here is the five:
    5
    that was the five
    </foo>


Base case: no function
----------------------

To run::

    python no_function_with_lxml.py

This just injects the 5, without using a custom function of any sort.


With lxml
---------

::

   python with_lxml.py

This constructs a custom function and makes it available to call
within the stylesheet, when lxml is used.

See <http://lxml.de/extensions.html#xpath-extension-functions>.


With Xalan: call a static function of a Java class
--------------------------------------------------

::

   javac info/securityrules/with_xalan_java_func/FiveClass.java
   jython with_xalan_java_func.py

We define a Java class, with a static method, and call our method from
inside the XSLT stylesheet using Xalan. (This assumes that Xalan is
the processor implementing the Java API for XML Processing (JAXP) in
your Java install.)

See <http://xml.apache.org/xalan-j/extensions.html#java-namespace>


Use a Java function that uses JSR223 to run Python code
-------------------------------------------------------

::

   javac info/securityrules/xalan_jsr223/PythonFive.java
   jython xalan_jsr223.py

Here we have the five function implemented in Python, and we spawn a
new Python interpreter to run the code when the XSLT function is
called. Spinning up a new interpreter every time is probably horrible
and could be optimized; other than that, the only problem here is that
the function we are calling has no state. The original custom function
in Python searches for a return value and adds to a list of bad inputs
if no suitable value is found; the list belongs to an object. Being as
the five function seems to be running in a different Python
interpreter, that isn't an option here.

See <http://www.jython.org/jythonbook/en/1.0/JythonAndJavaIntegration.html#jsr-223>
